﻿using UnityEngine;
using System.Collections;

public class MenuGUI : MonoBehaviour {


    private Rect m_buttonPlace;
    private int m_buttonWidth;
    private int m_buttonHeight;

	// Use this for initialization
	void Start () {
        m_buttonWidth = 130;
        m_buttonHeight = 40;
        m_buttonPlace = new Rect((Screen.width - m_buttonWidth) / 2, (Screen.height - m_buttonHeight) / 2, m_buttonWidth, m_buttonHeight);
	}

    void OnGUI()
    {

        //load demo scene
        if (GUI.Button(m_buttonPlace, "Start Game (Demo)"))
        {
            Application.LoadLevel("FlightScene");
        }
    }

}
