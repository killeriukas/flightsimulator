﻿using UnityEngine;
using System.Collections;

public class CameraRotScript : MonoBehaviour {

    // Update is called once per frame
	void Update () {

        //rotate camera 20 degrees per second
        transform.Rotate(Vector3.up, Time.deltaTime * 20);

	}

}
