﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

    //game object you want to follow
    public GameObject m_target;

	// Use this for initialization
	void Start () {
	        
	}
	
	// Update is called once per frame
	void Update () {

        //set camera's position a little bit behind and upper the target position
        transform.position = m_target.transform.position;
        transform.position -= m_target.transform.forward * 5.0f;
        transform.position += m_target.transform.up;

        //always look at target
        transform.LookAt(m_target.transform);
	}

}
