﻿using UnityEngine;
using System.Collections;

public class FlightGUI : MonoBehaviour {

    private Player m_player;

    private Rect m_buttonRestart;
    private Rect m_ammoLabel;
    private Rect m_speedLabel;
    private Rect m_curPlanePos;
    private Rect m_engineOnLabel;
    private Rect m_deathSign;
    private GUIStyle m_deathSignStyle;

    //tutorial
    private Rect m_engineLabel;
    private Rect m_tiltLabel;
    private Rect m_rotateLabel;
    private Rect m_shootLabel;
    private Rect m_hideControlsLabel;
    private bool m_showControls;

	// Use this for initialization
	void Start () {
	    m_player = GetComponent<Player>();
        m_showControls = true;

        m_buttonRestart = new Rect(10, Screen.height - 60, 120, 40);
        m_ammoLabel = new Rect(10, 76, 200, 22);
        m_speedLabel = new Rect(10, 10, 200, 22);
        m_curPlanePos = new Rect(10, 32, 200, 22);
        m_engineOnLabel = new Rect(10, 54, 200, 22);
        m_deathSign = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 20, 100, 20);
        m_deathSignStyle = new GUIStyle();
        m_deathSignStyle.fontSize = 60;
        m_deathSignStyle.normal.textColor = Color.red;

        //tutorial
        m_engineLabel = new Rect(Screen.width - 150, 10, 130, 22);
        m_tiltLabel = new Rect(Screen.width - 150, 32, 130, 22);
        m_rotateLabel = new Rect(Screen.width - 150, 54, 130, 22);
        m_shootLabel = new Rect(Screen.width - 150, 76, 130, 22);
        m_hideControlsLabel = new Rect(Screen.width - 150, 98, 130, 22);

	}
	
	// Update is called once per frame
	void Update () {
	    
        if(Input.GetKeyUp(KeyCode.H)) {
            m_showControls = !m_showControls;
        }

	}


    private void OnGUI()
    {

        GUI.Label(m_speedLabel, "Speed: " + m_player.GetCurrentPlaneSpeed());
        GUI.Label(m_engineOnLabel, "Engine: " + (m_player.IsPlaneStateSet(Player.PlaneStates.PLANE_STATE_TURN_ENGINE_ON) ? "ON" : "OFF"));
        GUI.Label(m_ammoLabel, "Ammo: " + m_player.GetCurrentWeapon().GetAmmoCount() + "/" + Player.MAX_AMMO_COUNT);
        GUI.Label(m_curPlanePos, "Position: " + m_player.GetCurrentPlanePosition());

        //if player is dead, enable restart button
        if (m_player != null && m_player.IsPlayerDead())
        {
            GUI.Label(m_deathSign, "Crashed!", m_deathSignStyle);
            if (GUI.Button(m_buttonRestart, "Restart Flight"))
            {
                m_player.Respawn();
            }
        }

        if (m_showControls)
        {
            GUI.Label(m_engineLabel, "Engine On/Off: T");
            GUI.Label(m_tiltLabel, "Tilt Down/Up: W/S");
            GUI.Label(m_rotateLabel, "Rotate Left/Right: A/D");
            GUI.Label(m_hideControlsLabel, "Hide Info: H");
            GUI.Label(m_shootLabel, "Shoot: LCtrl");
        }

    }

}
