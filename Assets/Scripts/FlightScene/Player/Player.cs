﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class responsible for the main handling of the player.
/// </summary>

public class Player : MonoBehaviour {

    private const int MIN_FLY_SPEED = 35;                         //minimum take off speed
    private const int DEATH_SPEED = 70;                           //collide with the ground at DEATH_SPEED and you have won a cookie :)  
    private const float MAX_PLANE_SPEED_IN_AIR = 150.0f;          //max plane speed - in air
    private const float MAX_PLANE_SPEED_ON_GROUND = 75.0f;        //max plane speed - on ground


    private const float MAX_ROTATION_SPEED_ON_GROUND = 40.0f;     //max rotation speed while on the ground
    private const float MAX_ROTATION_SPEED_IN_AIR = 20.0f;        //max rotation speed while in the air
    private const int MAX_PLANE_FORCE_FORWARD = 30;               //max force plane has to move forward
    public static int MAX_AMMO_COUNT = 20;





    private float m_curPlaneForce;                  //real-time plane engines' force
    private float m_curPlaneSpeed;                  //real-time plane speed

    private int m_planeStates;                      //plane states, which can be active more than one at a time
    private MainPlainStates m_mainPlaneStates;      //main plane state, where only one can be active at a time (for example: in air, or on the ground)

    private AWeapon m_curWeapon;                    //current weapon mounted on the player's plane

    //for respawn
    private Vector3 m_startPosition;                
    private Quaternion m_startRotation;

    //this state can be active only one at a time
    public enum MainPlainStates
    {
        MAIN_PLANE_STATE_ON_GROUND      =    0,
        MAIN_PLANE_STATE_IN_AIR         =    1,
        MAIN_PLANE_STATE_DEAD           =    2
    }

    //possible more than one state at a time
    public enum PlaneStates
    {
        //1 byte
        PLATE_STATE_HAS_FLY_SPEED           =      1 << 0,
        PLANE_STATE_TURN_ENGINE_ON          =      1 << 1,
        PLANE_STATE_MOVE_LEFT               =      1 << 2,
        PLANE_STATE_MOVE_RIGHT              =      1 << 3,
        PLANE_STATE_MOVE_UP                 =      1 << 4,
        PLANE_STATE_MOVE_DOWN               =      1 << 5,
        PLANE_STATE_SHOOTING                =      1 << 6
    }

    public float GetCurrentMaxPlaneSpeed()
    {
        return m_mainPlaneStates == MainPlainStates.MAIN_PLANE_STATE_IN_AIR ? MAX_PLANE_SPEED_IN_AIR : MAX_PLANE_SPEED_ON_GROUND;
    }
    public float GetCurrentMaxPlaneRotationSpeed()
    {
        return m_mainPlaneStates == MainPlainStates.MAIN_PLANE_STATE_IN_AIR ? MAX_ROTATION_SPEED_IN_AIR : MAX_ROTATION_SPEED_ON_GROUND;
    }

    public float GetCurrentMaxPlaneSpeedInPercent()
    {
        return m_curPlaneSpeed / GetCurrentMaxPlaneSpeed();
    }
    public float GetCurrentPlaneRotationSpeed()
    {
        return GetCurrentMaxPlaneRotationSpeed() * GetCurrentMaxPlaneSpeedInPercent();
    }

    public int GetCurrentPlaneSpeed()
    {
        return (int)(m_curPlaneSpeed * 350 / MAX_PLANE_SPEED_IN_AIR);
    }
    public Vector3 GetCurrentPlanePosition()
    {
        return transform.position;
    }


    public bool IsPlaneStateSet(PlaneStates planeState)
    {
        bool isSet = (m_planeStates & (int)planeState) > 0;

        if (isSet)
        {
            return true;
        }

        return false;
    }
    public void SetPlaneState(PlaneStates planeState)
    {
        if(IsPlaneStateSet(planeState))
        {
            return;
        }

        m_planeStates |= (int)planeState;
    }
    public void UnsetPlaneState(PlaneStates planeState)
    {
        if (!IsPlaneStateSet(planeState))
        {
            return;
        }

        m_planeStates &= ~(int)planeState;
    }
    public void SwitchPlaneState(PlaneStates planeState)
    {
        m_planeStates ^= (int)planeState;
    }

	// Use this for initialization
	void Start () {
        m_curWeapon = new GatlingGun(MAX_AMMO_COUNT);
        m_startPosition = transform.position;
        m_startRotation = transform.rotation;
        Respawn();
	}

    private void FixedUpdate()
    {
        //shall try to put all the force related stuff here
    }

	// Update is called once per frame
	void Update () {

        UpdateInput();
        UpdateGameLogic();

	}

    private void UpdateGameLogic()
    {

        //update weapon status
        m_curWeapon.UpdateWeapon();

        switch (m_mainPlaneStates)
        {
            case MainPlainStates.MAIN_PLANE_STATE_ON_GROUND:
            case MainPlainStates.MAIN_PLANE_STATE_IN_AIR:

                //calculate new speed (it's a magnitude of velocity)
                m_curPlaneSpeed = rigidbody.velocity.magnitude;

                if (IsPlaneStateSet(PlaneStates.PLANE_STATE_TURN_ENGINE_ON))
                {
                    rigidbody.useGravity = false;
                    rigidbody.drag = 0.3f;

                    //speed up, for now
                    SpeedUp();

                }
                else
                {
                    //speed down, for now
                    SpeedDown();
                }

                //enable or disable flight mode
                CheckFlightMode();

                //move plane right and left, depending on its current state
                MoveToSides();

                //move plane up and down, if allowed
                MoveUpDown();

                if(IsPlaneStateSet(PlaneStates.PLANE_STATE_SHOOTING))
                {
                    m_curWeapon.Shoot();
                }
                

                break;
            case MainPlainStates.MAIN_PLANE_STATE_DEAD:



                break;
            default:
                Debug.LogError("Error: UpdateGameLogic() main plane state unrecognized!");
                break;
        }

    }

    private void MoveUpDown()
    {
        //if plane is not in the air, check if we want to take off
        if (m_mainPlaneStates != MainPlainStates.MAIN_PLANE_STATE_IN_AIR)
        {

            //if user doesn't want to fly up or fly speed is insufficient, just return
            if (!IsPlaneStateSet(PlaneStates.PLATE_STATE_HAS_FLY_SPEED) || !IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_UP))
            {
                return;
            }

            //if plane wants to move up, set its state to in air
            m_mainPlaneStates = MainPlainStates.MAIN_PLANE_STATE_IN_AIR;
        }

        //if player wants to fly up, rotate it, i.e. forward vector changes depending on this
        if(IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_UP))
        {
            rigidbody.AddTorque(transform.right * (-GetCurrentPlaneRotationSpeed() * 0.05f), ForceMode.Force);
        }

        //if played wants to fly down, rotate it, i.e. forward vector changes depending on this
        if (IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_DOWN))
        {
            rigidbody.AddTorque(transform.right * GetCurrentPlaneRotationSpeed() * 0.05f, ForceMode.Force);
        }
    }

    private void CheckFlightMode()
    {
        if (m_curPlaneSpeed > MIN_FLY_SPEED)
        {
            SetPlaneState(PlaneStates.PLATE_STATE_HAS_FLY_SPEED);
        }
        else
        {
            UnsetPlaneState(PlaneStates.PLATE_STATE_HAS_FLY_SPEED);
        }
    }

    private void MoveToSides()
    {

        switch (m_mainPlaneStates)
        {
            case MainPlainStates.MAIN_PLANE_STATE_ON_GROUND:

                if(IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_RIGHT))
                {
                    rigidbody.AddForce(transform.right * GetCurrentPlaneRotationSpeed(), ForceMode.Force);
                }

                if (IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_LEFT))
                {
                    rigidbody.AddForce(transform.right * (-GetCurrentPlaneRotationSpeed()), ForceMode.Force);
                }

                break;
            case MainPlainStates.MAIN_PLANE_STATE_IN_AIR:

                //rotate clockwise
                if (IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_RIGHT))
                {
                    rigidbody.AddTorque(transform.forward * (-GetCurrentPlaneRotationSpeed() * 0.01f), ForceMode.Force);
                }

                //rotate counter-clockwise
                if (IsPlaneStateSet(PlaneStates.PLANE_STATE_MOVE_LEFT))
                {
                    rigidbody.AddTorque(transform.forward * GetCurrentPlaneRotationSpeed() * 0.01f, ForceMode.Force);
                }

                break;
            default:
                Debug.LogError("Error: MoveToSides() main plane state unrecognized!");
                break;
        }
    }

    private void SpeedDown()
    {
        //force reduction
        m_curPlaneForce -= Time.deltaTime * 20.0f;

        if (m_curPlaneForce < 0)
        {
            rigidbody.useGravity = true;
            m_curPlaneForce = 0.0f;
            rigidbody.drag = 0.0f;
        }

        if (m_curPlaneSpeed < 0)
        {
            m_curPlaneSpeed = 0.0f;
        }
    }

    private void SpeedUp()
    {

        //add delta force
        m_curPlaneForce += Time.deltaTime * 20.0f;

        //if plane force is bigger than the maximum allowed, clamp it
        if (m_curPlaneForce > MAX_PLANE_FORCE_FORWARD)
        {
            m_curPlaneForce = MAX_PLANE_FORCE_FORWARD;
        }

        //calculate force proportion for different axis
        Vector3 force = transform.forward * m_curPlaneForce;

        switch(m_mainPlaneStates) {
            case MainPlainStates.MAIN_PLANE_STATE_ON_GROUND:

                //clamp current plane speed within its limits while on the ground
                if (m_curPlaneSpeed > MAX_PLANE_SPEED_ON_GROUND)
                {
                    m_curPlaneSpeed = MAX_PLANE_SPEED_ON_GROUND;
                }
                else //increase the speed of the plane
                {
                    rigidbody.AddForce(force, ForceMode.Force);
                }

                break;
            case MainPlainStates.MAIN_PLANE_STATE_IN_AIR:

                //clamp current plane speed within its limits while in air
                if (m_curPlaneSpeed > MAX_PLANE_SPEED_IN_AIR)
                {
                    m_curPlaneSpeed = MAX_PLANE_SPEED_IN_AIR;
                }
                else //increase the speed of the plane
                {
                    rigidbody.AddForce(force, ForceMode.Force);
                }

                break;
            default:
                Debug.LogError("Error: SpeedUp() main plane state unrecognized!");
                break;
        }
    }

    private void UpdateInput()
    {
        //would need to abstract input for the full project

        //turn on/off engine
        if(Input.GetKeyUp(KeyCode.T))
        {
            SwitchPlaneState(PlaneStates.PLANE_STATE_TURN_ENGINE_ON);
        }

        //fly up - if allowed
        if(Input.GetKeyDown(KeyCode.S))
        {
            SetPlaneState(PlaneStates.PLANE_STATE_MOVE_UP);
        }

        if(Input.GetKeyUp(KeyCode.S))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_UP);
        }
        //fly up - ends

        //fly down - if allowed
        if(Input.GetKeyDown(KeyCode.W))
        {
            SetPlaneState(PlaneStates.PLANE_STATE_MOVE_DOWN);
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_DOWN);
        }
        //fly down - ends

        //rotate counter - clockwise
        if(Input.GetKeyDown(KeyCode.A))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_RIGHT);
            SetPlaneState(PlaneStates.PLANE_STATE_MOVE_LEFT);
        } 

        if (Input.GetKeyUp(KeyCode.A))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_LEFT);
        }

        //rotate clockwise
        if (Input.GetKeyDown(KeyCode.D))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_LEFT);
            SetPlaneState(PlaneStates.PLANE_STATE_MOVE_RIGHT);
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_MOVE_RIGHT);
        }

        //enable shooting action
        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            SetPlaneState(PlaneStates.PLANE_STATE_SHOOTING);
        }

        //disable shooting action
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            UnsetPlaneState(PlaneStates.PLANE_STATE_SHOOTING);
        }

    }

    public AWeapon GetCurrentWeapon()
    {
        return m_curWeapon;
    }

    public void KillPlayer()
    {
        m_mainPlaneStates = MainPlainStates.MAIN_PLANE_STATE_DEAD;
        m_planeStates = 0;
        rigidbody.drag = 0.3f;
        rigidbody.useGravity = true;
    }

    public bool IsPlayerDead()
    {
        return m_mainPlaneStates == MainPlainStates.MAIN_PLANE_STATE_DEAD;
    }

    private void OnCollisionEnter(Collision collider)
    {
        //if player hits the ground with faster than DEATH SPEED, kill player
        if (collider.gameObject.name == "Ground" && m_curPlaneSpeed > DEATH_SPEED)
        {
            KillPlayer();
        }
    }

    public void Respawn()
    {

        //reset all the values for the start game
        m_planeStates = 0;
        m_curPlaneForce = 0.0f;
        m_curPlaneSpeed = 0.0f;
        rigidbody.velocity = Vector3.zero;
        m_mainPlaneStates = MainPlainStates.MAIN_PLANE_STATE_ON_GROUND;
        m_curWeapon.SetAmmo(MAX_AMMO_COUNT);
        transform.position = m_startPosition;
        transform.rotation = m_startRotation;
    }
}
