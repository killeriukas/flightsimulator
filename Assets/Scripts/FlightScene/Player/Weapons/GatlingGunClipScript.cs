﻿using UnityEngine;
using System.Collections;

public class GatlingGunClipScript : MonoBehaviour {

    public enum ClipState
    {
        CLIP_STATE_ALIVE = 0,
        CLIP_STATE_DEAD
    }


    private float m_lifeTime;
    private ClipState m_curClipState;

	// Use this for initialization
	void Start () {
        m_curClipState = ClipState.CLIP_STATE_ALIVE;
        m_lifeTime = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {

        switch (m_curClipState)
        {
            case ClipState.CLIP_STATE_ALIVE:
                m_lifeTime -= Time.deltaTime;

                if (m_lifeTime < 0)
                {
                    m_curClipState = ClipState.CLIP_STATE_DEAD;
                }
                break;

            case ClipState.CLIP_STATE_DEAD:
                Destroy(gameObject);
                break;

            default:
                Debug.LogError("Error: Update() GatlingGunClip error clip state!");

                break;
        }
	    
	}

    void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.name == "Ground")
        {
            m_curClipState = ClipState.CLIP_STATE_DEAD;
        }
    }

}
