﻿using UnityEngine;
using System.Collections;

public class GatlingGun : AWeapon {

    private GameObject m_forwardObject;                 //forward vector of this object is used for bullets' shoot direction
    private const float GAP_BETWEEN_SHOOTS = 0.2f;
    private float m_curTime;                            //real-time between shoots

    public GatlingGun()
        : base("CenterGatlingGun", "Models/Weapons/GatlingClip")
    {
        m_curTime = GAP_BETWEEN_SHOOTS;
        m_forwardObject = (GameObject)GameObject.Find("Boeing747");
    }

    public GatlingGun(int ammo)
        : base(ammo, "CenterGatlingGun", "Models/Weapons/GatlingClip")
    {
        m_curTime = GAP_BETWEEN_SHOOTS;
        m_forwardObject = (GameObject)GameObject.Find("Boeing747");
    }

    public override void UpdateWeapon()
    {
        base.UpdateWeapon();

        if (m_curTime < GAP_BETWEEN_SHOOTS)
        {
            m_curTime += Time.deltaTime;
        }
    }

    public override void Shoot()
    {
        //if ammo is empty, just return
        if (IsEmpty() || m_curTime < GAP_BETWEEN_SHOOTS)
        {
            return;
        }

        m_curTime = 0.0f;

        ReduceAmmo();

        //create ammo visual and give direction and force to it
        GameObject clip = (GameObject) GameObject.Instantiate(m_ammoVisual, m_startPosObject.transform.position, Quaternion.identity);
        clip.rigidbody.AddForce(m_forwardObject.transform.forward * 150, ForceMode.Impulse);

    }
}
