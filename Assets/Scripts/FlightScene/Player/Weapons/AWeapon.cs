﻿using UnityEngine;
using System.Collections;

public abstract class AWeapon {

    private int m_ammo;
    protected GameObject m_ammoVisual;              //ammo visualization object (the thing you see in a game as a bullet)
    protected GameObject m_startPosObject;          //weapon shoot start position

    public int GetAmmoCount()
    {
        return m_ammo;
    }

    public AWeapon(string startPos, string modelPath)
    {
        m_ammoVisual = (GameObject)Resources.Load(modelPath);
        m_startPosObject = (GameObject)GameObject.Find(startPos);
        m_ammo = 10;
    }

    public AWeapon(int ammo, string startPos, string modelPath)
    {
        m_ammoVisual = (GameObject) Resources.Load(modelPath);
        m_startPosObject = (GameObject)GameObject.Find(startPos);
        m_ammo = ammo;
    }

    public bool IsEmpty()
    {
        return m_ammo == 0;
    }

    protected void ReduceAmmo()
    {
        if (!IsEmpty())
        {
            --m_ammo;
        }
    }

    public void SetAmmo(int ammo)
    {
        m_ammo = ammo;
    }

    //implement this in children
    public abstract void Shoot();

    //implement this in children, if needed
    public virtual void UpdateWeapon()
    {

    }
}
